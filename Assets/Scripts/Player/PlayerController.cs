using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


/// <summary>
/// 플레이어 캐릭터를 조작하는 기능을 가진 컴포넌트
/// </summary>
public class PlayerController : MonoBehaviour
{
    public MapGenerator m_MapGenerator;

    /// <summary>
    /// 플레이어 캐릭터 컴포넌트
    /// </summary>
    private PlayerCharacter _ControlledCharacter;

    /// <summary>
    /// 뒤로 움직인 횟수
    /// </summary>
    private int _BackMoveCount;

    private void Awake()
    {
        _ControlledCharacter = GetComponent<PlayerCharacter>();
    }

    private void OnFront()
    {
        _ControlledCharacter?.OnFrontInput();
        // 땅에 닿아있지 않는 경우 호출 취소
        if (!_ControlledCharacter.playerMovement.isGrounded) return;
        // 앞쪽에 장애물이 있는 경우 장애물 방향으로 이동 불가
        if (!_ControlledCharacter.playerMovement.isFrontBlock)
        {
            m_MapGenerator.MoveForward();

            ++GameManager.getInstance.m_ScoreData.m_Score;

            _ControlledCharacter.m_GameSceneUI.UpdateScore();

        }
    }

    private void OnBack()
    {
        _ControlledCharacter?.OnBackInput();
        if (!_ControlledCharacter.playerMovement.isGrounded) return;
        // 뒤쪽에 장애물이 있는 경우 장애물 방향으로 이동 불가
        if (!_ControlledCharacter.playerMovement.isBackBlock)
        {
            m_MapGenerator.MoveBackward();

            // 뒤로 움직일 때 마다 카운트 1씩증가
            ++_BackMoveCount;
            // 카운트 3인 경우 게임 오버
            if (_BackMoveCount == 3) _ControlledCharacter.GameOver();
        }
    }

    private void OnLeft()
    {
        _ControlledCharacter?.OnLeftInput();
        if (!_ControlledCharacter.playerMovement.isGrounded) return;
        // 왼쪽에 장애물이 있는 경우 장애물 방향으로 이동 불가
        if (!_ControlledCharacter.playerMovement.isLeftBlock)
        {
            m_MapGenerator.MoveLeft();
        }
    }


    private void OnRight()
    {
        _ControlledCharacter?.OnRightInput();
        if (!_ControlledCharacter.playerMovement.isGrounded) return;
        // 오른쪽에 장애물이 있는 경우 장애물 방향으로 이동 불가
        if (!_ControlledCharacter.playerMovement.isRightBlock)
        {
            m_MapGenerator.MoveRight();
        }

    }
}
