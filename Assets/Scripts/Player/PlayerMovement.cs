using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("# 점프 힘")]
    public float m_JumpPower = 3.0f;

    /// <summary>
    /// 점프 입력이 들어왔음을 나타냅니다.
    /// </summary>
    private bool _IsJump;

    /// <summary>
    /// 땅에 닿아있음을 나타냅니다
    /// </summary>
    private bool _IsGrounded;


    /// <summary>
    /// 키입력 축 값
    /// </summary>
    private Vector3 _InputVelocity;

    /// <summary>
    /// 진행방향 이동 가능 여부
    /// </summary>
    private bool _IsFrontBlock;
    private bool _IsBackBlock;
    private bool _IsLeftBlock;
    private bool _IsRightBlock;

    /// <summary>
    /// 땟목 오브젝트
    /// </summary>
    private Raft _RaftObject;

    /// <summary>
    /// 뗏목 트리거와 피교할 뗏목 오브젝트 트랜스폼
    /// </summary>
    private Transform _RaftCompareObj;

    /// <summary>
    /// 땟목 위치 오프셋
    /// </summary>
    private Vector3 _RaftOffsetPos;


    /// <summary>
    /// 플레이어 박스 콜라이더
    /// </summary>
    private BoxCollider _BoxCollider;

    /// <summary>
    /// 플레이어 리지드바디 컴포넌트
    /// </summary>
    private Rigidbody _RigidBody;

    /// <summary>
    /// 플레이어 캐릭터 컴포넌트
    /// </summary>
    private PlayerCharacter _PlayerCharacter;

    /// <summary>
    /// 땅에 닿아있음을 나타내는 읽기 전용 프로퍼티
    /// </summary>
    public bool isGrounded => _IsGrounded;


    /// <summary>
    /// 진행 방향 이동 가능여부 읽기전용 프로퍼티
    /// </summary>
    public bool isFrontBlock => _IsFrontBlock;
    public bool isBackBlock => _IsBackBlock;
    public bool isLeftBlock => _IsLeftBlock;
    public bool isRightBlock => _IsRightBlock;

    private void Awake()
    {
        _BoxCollider = GetComponent<BoxCollider>();
        _RigidBody = GetComponent<Rigidbody>();
        _PlayerCharacter = GetComponent<PlayerCharacter>();
    }

    private void FixedUpdate()
    {
        // 플레이어 중심으로 4방향으로 레이를 쏩니다.
        _IsFrontBlock = IsCheckCollision(Vector3.forward);
        _IsBackBlock = IsCheckCollision(Vector3.back);
        _IsLeftBlock = IsCheckCollision(Vector3.left);
        _IsRightBlock = IsCheckCollision(Vector3.right);

        // 땅에 닿아있는지 확인
        _IsGrounded = IsGrounded();

        // 탑승한 떗목 위치 확인
        UpdateRaft();
    }

    /// <summary>
    /// 점프를 합니다.
    /// </summary>
    /// <param name="isGrounded">땅에 닿아있는지 확인</param>
    private void Jump(bool isGrounded)
    {
        // 땅에 닿아있고
        if (isGrounded)
        {
            // 점프가 가능할 때
            if (_IsJump)
            {
                // 점프
                _IsJump = false;
                _RigidBody.AddForce(Vector3.up * m_JumpPower, ForceMode.Impulse);
            }
        }
    }

    /// <summary>
    /// 땅에 닿아있음을 확인합니다.
    /// </summary>
    /// <returns></returns>
    private bool IsGrounded()
    {
        RaycastHit hit;
        bool isHit = Physics.Raycast(transform.position, Vector3.down, out hit, 0.1f);
        return isHit;
    }



    /// <summary>
    /// 입력 축 값으로 캐릭터 회전
    /// </summary>
    private void RotateToDirection()
    {
        transform.forward = _InputVelocity;
    }

    /// <summary>
    /// 땟목 위치와 플레이어 위치를 동기화 합니다.
    /// </summary>
    private void UpdateRaft()
    {
        if (_RaftObject == null) return;

        Vector3 playerPos = _RaftObject.transform.position + _RaftOffsetPos;

        transform.position = playerPos;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Contains("Car") || other.tag.Contains("Water"))
        {
            _PlayerCharacter.GameOver();
        }       
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag.Contains("Raft"))
        {
            _IsGrounded = true;

            // 씬에서 땟목 오브젝트를 얻어옴
            _RaftObject = collision.transform.GetComponent<Raft>();

            // 땟목 오브젝트가 활성화 될때
            if (_RaftObject != null)
            {
                // 땟목의 트랜스폼 얻습니다.
                _RaftCompareObj = _RaftObject.transform;

                // 땟목과 플레이어의 차이를 오프셋에 담습니다.
                _RaftOffsetPos = transform.position - _RaftObject.transform.position;
            }
            // 뗏목 오브젝트가 사라질때
            else
            {
                // 값을 초기화
                _RaftCompareObj = null;
                _RaftObject = null;
                _RaftOffsetPos = Vector3.zero;
            }

        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.transform.tag.Contains("Raft"))
        {
            // 값 초기화
            _RaftCompareObj = null;
            _RaftObject = null;
            _RaftOffsetPos = Vector3.zero;
        }
    }


    /// <summary>
    /// 나무와 돌에 레이캐스트를 합니다.
    /// </summary>
    /// <param name="direction">레이를 쏠 방향</param>
    /// <returns></returns>
    private bool IsCheckCollision(Vector3 direction)
    {
        // 레이어 마스크
        int collisionLayerMask = LayerMask.GetMask("Environment");

        // 플레이어 중앙에서 레이 발사
        Vector3 playerOffset = new Vector3(0, _BoxCollider.size.y * 0.5f, 0);
        Vector3 origin = transform.position + playerOffset;

        RaycastHit[] hit = Physics.RaycastAll(origin, direction, 2.0f, collisionLayerMask);

        // 충돌체가 있다면 
        if (hit.Length != 0)
        {
            // 참으로 리턴
            return true;
        }
        return false;
    }

    /// <summary>
    /// 키보드 위쪽 키 입력시 호출
    /// </summary>
    public void OnFrontInput()
    {
        _InputVelocity = Vector3.forward;
        if (!_IsJump && _IsGrounded)
        {
            _IsJump = true;

            Jump(_IsGrounded);

            // 캐릭터 바라보는 방향설정
            RotateToDirection();
        }
        _RaftOffsetPos += _InputVelocity * 2.0f;
    }

    /// <summary>
    /// 키보드 아래쪽 키 입력시 호출
    /// </summary>
    public void OnBackInput()
    {
        _InputVelocity = Vector3.back;
        if (!_IsJump && _IsGrounded)
        {
            _IsJump = true;

            Jump(_IsGrounded);


            // 캐릭터 바라보는 방향설정
            RotateToDirection();
        }
        _RaftOffsetPos += _InputVelocity * 2.0f;

    }

    /// <summary>
    /// 키보드 왼쪽 키 입력시 호출
    /// </summary>
    public void OnLeftInput()
    {
        _InputVelocity = Vector3.left;
        if (!_IsJump && _IsGrounded)
        {
            _IsJump = true;

            Jump(_IsGrounded);


            // 캐릭터 바라보는 방향설정
            RotateToDirection();
        }
        _RaftOffsetPos += _InputVelocity * 2.0f;

    }

    /// <summary>
    /// 키보드 오른쪽 키 입력시 호출
    /// </summary>
    public void OnRightInput()
    {
        _InputVelocity = Vector3.right;
        if (!_IsJump && _IsGrounded)
        {
            _IsJump = true;

            Jump(_IsGrounded);


            // 캐릭터 바라보는 방향설정
            RotateToDirection();
        }
        _RaftOffsetPos += _InputVelocity * 2.0f;

    }
}
