using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimController : MonoBehaviour
{
    private Animator _Animator;
    public Animator animator => _Animator ?? (_Animator = GetComponent<Animator>());

    /// <summary>
    /// 점프 애니메이션 실행을 위한 파라미터 설정
    /// </summary>
    public bool isGrounded { set => animator.SetBool("_IsGrounded",value); }
}
