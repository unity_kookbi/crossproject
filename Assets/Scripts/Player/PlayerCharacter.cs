using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour
{
    [Header("Game UI")]
    public GameSceneUI m_GameSceneUI;

    /// <summary>
    /// 이동 기능을 가지는 컴포넌트
    /// </summary>
    private PlayerMovement _PlayerMovement;

    /// <summary>
    /// 애니메이터 컴포넌트
    /// </summary>
    private PlayerAnimController _PlayerAnimController;

    public PlayerMovement playerMovement => _PlayerMovement ?? (_PlayerMovement = GetComponent<PlayerMovement>());

    public PlayerAnimController animController => _PlayerAnimController ?? (_PlayerAnimController = GetComponent<PlayerAnimController>());

    private void Update()
    {
        UpdateAnimControllerParam();
    }

    /// <summary>
    /// 애니메이션 컨트롤러 파라미터 값을 갱신합니다.
    /// </summary>
    private void UpdateAnimControllerParam()
    {
        animController.isGrounded = playerMovement.isGrounded;
    }

    /// <summary>
    /// 게임 오버 시 호출되는 메서드입니다.
    /// </summary>
    public void GameOver()
    {
        Time.timeScale = 0.0f;
        m_GameSceneUI.OnGameOver();
    }

    public void OnFrontInput() => playerMovement.OnFrontInput();

    public void OnBackInput() => playerMovement.OnBackInput();
    public void OnLeftInput() => playerMovement.OnLeftInput();

    public void OnRightInput() => playerMovement.OnRightInput();
}
