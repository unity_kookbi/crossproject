using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    [Header("# 맵 오브젝트")]
    public MapObject[] m_MapObjectArray;

    [Header("# Z 축 반대 방향 맵 생성 개수")]
    public int m_MinPosZ = -10;

    [Header("# Z 축 방향 맵 생성 개수")]
    public int m_MaxPosZ = 10;

    [Header("# 맵을 자식으로 둘 오브젝트")]
    public Transform m_ParentTransform;

    [Header("# 시작 맵 오브젝트")]
    public MapObject m_StartMap;

    /// <summary>
    /// 맵 오브젝트 컴포넌트
    /// </summary>
    private MapObject _MapObject;

    /// <summary>
    /// 생성될 맵들을 담는 리스트
    /// </summary>
    private List<MapObject> _MapList = new List<MapObject>();

    private void Start()
    {
        // 시작 맵 생성
        GenerateMap();
    }

    /// <summary>
    /// 맵 생성
    /// </summary>
    private void GenerateMap()
    {
        for (int i = m_MinPosZ; i < m_MaxPosZ + 1; ++i)
        {
            // 스타트 맵 생성
            if (i == 0)
            {
                // 시작 맵 복사 생성
                _MapObject = Instantiate(m_StartMap);

                // 리스트에 추가
                _MapList.Add(_MapObject);

                // 맵 z 축 위치 설정
                Vector3 offsetPos = Vector3.zero;
                offsetPos.z = i * 2.0f;

                // 설정한 오브젝트에 자식 오브젝트로 생성
                _MapObject.transform.SetParent(m_ParentTransform);

                // 생성되는 맵 위치 설정
                _MapObject.transform.position = offsetPos;
            }
            else
            {
                // 나머지 맵 생성
                CloneMap(i);
            }
        }
    }

    /// <summary>
    /// 맵 오브젝트 복사 생성
    /// </summary>
    /// <param name="posZ">z 축 생성위치</param>
    private void CloneMap(int posZ)
    {
        // 맵 오브젝트들 중 랜덤 선택
        int randomIndex = Random.Range(0, m_MapObjectArray.Length);

        // 랜덤 선택된 맵 복사 생성
        _MapObject = Instantiate(m_MapObjectArray[randomIndex]);

        _MapList.Add(_MapObject);

        // z 축 위치 설정
        Vector3 offsetPos = Vector3.zero;
        offsetPos.z = posZ * 2.0f;

        // 설정한 오브젝트에 자식 오브젝트로 생성
        _MapObject.transform.SetParent(m_ParentTransform);

        // 맵 위치 설정
        _MapObject.transform.position = offsetPos;

        // 맵 좌우 반전 여부 랜덤 뽑기
        int randomRotate = Random.Range(0, 2);

        // 값이 1일 시 
        if (randomRotate == 1)
        {
            // 맵 좌우 반전
            _MapObject.transform.rotation = Quaternion.Euler(0, 180f, 0);
        }

        _MapObject = null;
    }

    /// <summary>
    /// 윗 방향 키 입력 시 맵 추가 생성과 삭제
    /// </summary>
    /// <param name="mapIndex"></param>
    private void MapRemoveNGenerate(int mapIndex)
    {
        // 삭제할 맵
        MapObject removeMap = _MapList[mapIndex];

        // 삭제할 맵을 null 로 설정
        _MapList[mapIndex] = null;

        // 리스트 마지막 순서로 null 이동
        for (int i = 0; i < _MapList.Count - 1; ++i)
        {
            // 빈 공간을 발견한 경우
            if (_MapList[i] == null)
            {
                // 가장 가까운 다음 요소를 찾고, 빈 공간과 위치를 교환합니다.
                for (int j = (i + 1); j < _MapList.Count; ++j)
                {
                    // 비어있지 않은 라인 그룹 객체를 발견한 경우
                    if (_MapList[j] != null)
                    {
                        // 서로 위치를 교환합니다.
                        _MapList[i] = _MapList[j];
                        _MapList[j] = null;

                        // 빈 공간과 교체된 라인 그룹의 인덱스를 설정합니다.
                        _MapList[i].SetMapIndex(i);

                        // 빈 공간과 교체가 끝났으므로 다음 교환을 진행합니다.
                        break;
                    }
                }
            }
        }
        // 삭제할 맵 오브젝트 파괴
        Destroy(removeMap.gameObject);

        // 리스트에 빈 인덱스 설정
        int emptyIndex = GetEmptyMapIndex();

        // 빈 인덱스 못찾았을 경우 리턴
        if (emptyIndex == -1) return;

        // 재생성할 맵의 z 위치 설정
        int nextGeneratePosZ = (int)_MapList[emptyIndex - 1].transform.position.z + 2;

        // 재생성할 맵의 x 위치 설정
        int nextGeneratePosX = (int)_MapList[emptyIndex - 1].transform.position.x;

        // 리스트의 빈 인덱스에 재생성 맵 설정
        _MapList[emptyIndex] = GenerateMap(nextGeneratePosX,nextGeneratePosZ);

    }

    /// <summary>
    /// 리스트에서 null 인덱스 찾는 메서드
    /// </summary>
    /// <returns></returns>
    private int GetEmptyMapIndex()
    {
        for (int i = _MapList.Count - 1; i > -1; --i)
        {
            if (_MapList[i] == null) return i;
        }

        return -1;

    }

    /// <summary>
    /// 맵을 재생성합니다.
    /// </summary>
    /// <param name="posX">재생성할 맵의 z 위치</param>
    /// <param name="posZ">재생성할 맵의 x 위치</param>
    /// <returns>재생성한 맵오브젝트를 반환</returns>
    private MapObject GenerateMap(int posX,int posZ)
    {
        // 맵 오브젝트들 중 랜덤 선택
        int randomIndex = Random.Range(0, m_MapObjectArray.Length);

        // 랜덤 선택된 맵 복사 생성
        _MapObject = Instantiate(m_MapObjectArray[randomIndex]);

        Vector3 offsetPos = Vector3.zero;
        offsetPos.z = posZ;

        offsetPos.x = posX;

        // z 축 위치 설정
        _MapObject.transform.position = offsetPos;

        // 설정한 오브젝트에 자식 오브젝트로 생성
        _MapObject.transform.SetParent(m_ParentTransform);

        // 맵 좌우 반전 여부 랜덤 뽑기
        int randomRotate = Random.Range(0, 2);

        // 값이 1일 시 
        if (randomRotate == 1)
        {
            // 맵 좌우 반전
            _MapObject.transform.rotation = Quaternion.Euler(0, 180f, 0);
        }

        return _MapObject;
    }

    /// <summary>
    /// 윗 방향키 입력시 맵이 뒤로 이동
    /// 첫번째 맵 삭제 그리고 끝에 맵 재생성
    /// 플레이어 앞으로 이동하는 것 처럼 보임
    /// </summary>
    public void MoveForward()
    {
        foreach (MapObject mapObject in _MapList)
        {
            mapObject.MapMoveForward();
        }
        
        MapRemoveNGenerate(0);
    }

    /// <summary>
    /// 뒷 방향키 입력시 맵이 앞으로 이동
    /// 플레이어 뒤로 이동하는것 처럼 보임
    /// </summary>
    public void MoveBackward()
    {
        foreach (MapObject mapObject in _MapList)
        {
            mapObject.MapMoveBackWard();
        }

    }
    
    /// <summary>
    /// 왼쪽방향키 입력시 맵이 오른쪽으로 이동
    /// 플레이어 왼쪽으로 이동하는것처럼 보임
    /// </summary>
    public void MoveLeft()
    {
        foreach (MapObject mapObject in _MapList)
        {
            mapObject.MapMoveLeft();
        }
    }

    /// <summary>
    /// 오른쪽 방향키 입력시 맵이 왼쪽으로 이동
    /// 플레이어 오른쪽으로 이동하는 것처럼 보임
    /// </summary>
    public void MoveRight()
    {
        foreach (MapObject mapObject in _MapList)
        {
            mapObject.MapMoveRight();
        }
    }
}
