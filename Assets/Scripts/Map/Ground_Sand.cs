using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground_Sand : MapObject
{
    [Header("# 조형물 프리팹")]
    public List<GameObject> m_Environments;

    [Header("# 조형물 생성 퍼센트")]
    public int m_SpawnCreateRandom = 10;

    [Header("# 조형물 위치 오프셋")]
    public Vector3 offset;

    /// <summary>
    /// 맵 x 축 최소 위치
    /// </summary>
    private int m_MapMinX = -50;

    /// <summary>
    /// 맵 x 축 최대 위치
    /// </summary>
    private int m_MapMaxX = 50;

    private void Start()
    {
        GenerateEnvironment();
    }

    /// <summary>
    /// 조형물 복사 생성
    /// </summary>
    private void GenerateEnvironment()
    {
        // 범위는 맵 x 축 전범위
        for(int i = m_MapMinX; i < m_MapMaxX; ++i)
        {
            // 생성 확률 설정
            int randomVal = Random.Range(0, 100);
            if( randomVal < m_SpawnCreateRandom)
            {
                // 조형물 (ex. 나무) 랜덤 설정
                int randomIndex = Random.Range(0,m_Environments.Count);

                // 조형물 복사 생성
                GameObject environment = Instantiate(m_Environments[randomIndex]);

                // 조형물 위치 설정
                Vector3 spawnPosition = new Vector3Int(i, 1, 0);

                environment.transform.SetParent(transform);
                environment.transform.localPosition = spawnPosition + offset;
            }
        }

    }
}
