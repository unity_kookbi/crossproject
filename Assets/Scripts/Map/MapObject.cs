using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapObject : MonoBehaviour
{
    /// <summary>
    /// 라인 그룹 인덱스를 나타냅니다.
    /// 가장 위에 위치한 그룹이 0 번으로 사용됩니다.
    /// 해당 인덱스에 따라 라인 그룹이 배치되는 위치가 결정되도록 합니다.
    /// </summary>
    private int _MapIndex;

    /// <summary>
    /// 라인 그룹 인덱스를 설정합니다.
    /// </summary>
    /// <param name="mapIndex">설정시킬 인덱스를 전달합니다.</param>
    public void SetMapIndex(int mapIndex)
    {
        _MapIndex = mapIndex;
    }

    public void MapMoveForward()
    {
        transform.position += Vector3.back * 2.0f;
    }

    public void MapMoveBackWard()
    {
        transform.position += Vector3.forward * 2.0f;
    }

    public void MapMoveLeft()
    {
        transform.position += Vector3.right * 2.0f;
    }

    public void MapMoveRight()
    {
        transform.position += Vector3.left * 2.0f;
    }

}
