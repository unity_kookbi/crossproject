using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MapObject
{
    [Header("# ���� ������Ʈ ������")]
    public Raft m_RaftPrefab;

    [Header("# ������ ��ġ")]
    public Transform m_GenerationPos;

    [Header("# ������ Ȯ��")]
    public int m_GenerationPercent;

    [Header("# ���� �ֱ�")]
    public float m_CloneDelaySec = 1.0f;

    [Header("# �¸� �ּ� �ӷ�")]
    public float m_MinRaftSpeed;

    [Header("# �¸� �ְ� �ӷ�")]
    public float m_MaxRaftSpeed;

    /// <summary>
    /// ���� ������ �ð�
    /// </summary>
    private float m_NextSecToClone = 0.0f;

    /// <summary>
    /// �� �� �¸� �ӷ� �б� ���� ������Ƽ
    /// </summary>
    public float m_RaftSpeedByMap { get; private set; }



    private void Start()
    {
        // �� �� �� �ӷ��� �������� ����
        m_RaftSpeedByMap = Random.Range(m_MinRaftSpeed, m_MaxRaftSpeed);
    }


    private void Update()
    {
        float currentTime = Time.time;

        // �÷��� �ð����� ���� ������ �ð��� �۴ٸ�
        if (m_NextSecToClone <= currentTime)
        {
            // ���� Ȯ�� ����
            int generationPercent = Random.Range(0, 100);
            if (generationPercent <= m_GenerationPercent)
            {
                // ���� ����
                CloneRaft();
            }

            // ���� ���� �ð� ����
            m_NextSecToClone = currentTime + m_CloneDelaySec;
        }
    }

    /// <summary>
    /// ���� ������Ʈ �������
    /// </summary>
    private void CloneRaft()
    {
        // ������ ��ġ ����
        Transform clonePos = m_GenerationPos;

        // ������ ����
        Vector3 offset = clonePos.position;
        offset.y = 1f;

        // ���� ����
        GameObject cloneCar = Instantiate(m_RaftPrefab.gameObject,
            offset,
            m_GenerationPos.rotation,
            transform);
        // ������Ʈ Ȱ��ȭ
        cloneCar.SetActive(true);
    }
}
