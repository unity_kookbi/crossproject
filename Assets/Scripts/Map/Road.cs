using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 로드맵 컴포넌트
/// </summary>
public class Road : MapObject
{
    [Header("# 자동차 오브젝트 프리팹")]
    public Car[] m_CarPrefabs;

    [Header("# 생성할 위치")]
    public Transform m_GenerationPos;
    
    [Header("# 생성할 확률")]
    public int m_GenerationPercent;
    
    [Header("# 생성 주기")]
    public float m_CloneDelaySec = 1.0f;

    [Header("# 차 최소 속력")]
    public float m_MinCarSpeed;

    [Header("# 차 최고 속력")]
    public float m_MaxCarSpeed;

    /// <summary>
    /// 맵 당 차 속력 읽기 전용 프로퍼티
    /// </summary>
    public float m_CarSpeedByMap { get; private set; }

    /// <summary>
    /// 복사 생성할 자동차 오브젝트
    /// </summary>
    private Car m_CloneCar;

    /// <summary>
    /// 다음 생성할 시간
    /// </summary>
    private float m_NextSecToClone = 0.0f;

    private void Start()
    {
        // 맵 당 차 속력을 랜덤으로 설정
        m_CarSpeedByMap = Random.Range(m_MinCarSpeed, m_MaxCarSpeed);
    }

    private void Update()
    {
        float currentTime = Time.time;

        // 플레이 시간보다 다음 생성할 시간이 작다면
        if(m_NextSecToClone <= currentTime)
        {
            // 생성 확률 설정
            int generationPercent = Random.Range(0, 100);
            if(generationPercent <= m_GenerationPercent)
            { 
                // 복사 생성
                CloneCar();
            }

            // 다음 생성 시간 설정
            m_NextSecToClone = currentTime + m_CloneDelaySec;
        }
    }

    /// <summary>
    /// 자동차 오브젝트 복사생성
    /// </summary>
    private void CloneCar()
    {
        // 프리팹 리스트에서 생성할 오브젝트를 랜덤 설정
        int randomIndex = Random.Range(0, m_CarPrefabs.Length);
        m_CloneCar = m_CarPrefabs[randomIndex];

        // 생성할 위치 설정
        Transform clonePos = m_GenerationPos;

        // 오프셋 설정
        Vector3 offset = clonePos.position;
        offset.y = 1.0f;

        // 복사 생성
        GameObject cloneCar = Instantiate(m_CloneCar.gameObject,
            offset,
            m_GenerationPos.rotation,
            transform);
        // 오브젝트 활성화
        cloneCar.SetActive(true);
    }

}
