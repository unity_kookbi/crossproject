using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameSceneUI : MonoBehaviour
{
    [Header("점수 텍스트")]
    public TMP_Text m_ScoreText;

    [Header("메인신 버튼")]
    public Button m_GameOverButton;

    [Header("게임오버 텍스트")]
    public TMP_Text m_GameOverText;

    private void Awake()
    {
        m_GameOverButton.onClick.AddListener(OnGoToMainButtonClicked);

        m_GameOverButton.gameObject.SetActive(false);

        m_GameOverText.gameObject.SetActive(false);
    }

    private void OnGoToMainButtonClicked()
    {
        Time.timeScale = 1.0f;

        // 최고 점수 갱신
        GameManager.getInstance.UpdateBestScore();

        SceneManager.LoadScene("MainScene");
    }

    public void OnGameOver()
    {
        // 버튼 활성화
        m_GameOverButton.gameObject.SetActive(true);

        // 텍스트 활성화
        m_GameOverText.gameObject.SetActive(true);

    }

    public void UpdateScore()
    {
        m_ScoreText.text = "SCORE : " + GameManager.getInstance.m_ScoreData.m_Score.ToString();
    }


}
