

public class Constants
{
    /// <summary>
    /// 저장 파일 경로를 나타냅니다.
    /// </summary>
    public static string SAVE_DATA_PATH =>
        $"{UnityEngine.Application.dataPath}\\Resources\\SaveData\\";

    /// <summary>
    /// 저장 파일 이름입니다.
    /// </summary>
    public static string SAVE_DATA_FILENAME =>
        $"SaveData.json";

}