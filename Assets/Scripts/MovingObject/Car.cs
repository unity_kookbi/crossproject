using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    [Header("# 오브젝트 파괴 위치")]
    public float m_LangeMap = 49.0f;

    /// <summary>
    /// 차 속력
    /// </summary>
    private float _Speed;

    /// <summary>
    /// Road 컴포넌트
    /// </summary>
    private Road _Road;

    private void Awake()
    {
        _Road = GetComponentInParent<Road>();
    }

    private void FixedUpdate()
    {
        // x 축 방향으로 이동
        MoveX();

        // 오브젝트 파괴
        DestroyObject();
    }

    /// <summary>
    /// x 축 방향으로 이동합니다.
    /// </summary>
    private void MoveX()
    {
        _Speed = _Road.m_CarSpeedByMap;
        float moveX = _Speed * Time.fixedDeltaTime;

        transform.Translate(moveX, 0.0f, 0.0f);
    }

    /// <summary>
    /// 일정 위치 도달시 오브젝트를 파괴합니다
    /// </summary>
    private void DestroyObject()
    {
        if (transform.localPosition.x >= m_LangeMap)
        {
            Destroy(gameObject);
        }
    }

}
